﻿using SignalAlgorithms.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SignalAlgorithms.Algorithms
{
    class AmplitudeAlgorithms
    {
        private int _generatorSamplingRate;
        public AmplitudeAlgorithms(int generatorSamplingRate)
        {
            _generatorSamplingRate = generatorSamplingRate;
        }

        public double UseSignalDelay(List<SignalSample> signalSamples, int frequency)
        {
            List<double> amplitudeSamples = new List<double>();
            double generatorPeriodTime = (double)1 / _generatorSamplingRate;
            for (var i = 2; i < signalSamples.Count; i++)
            {
                amplitudeSamples.Add(Math.Sqrt(
                    (2 * (Math.Pow(signalSamples[i - 1].Value, 2) - signalSamples[i].Value * signalSamples[i - 2].Value)) /
                    (1 - Math.Cos(4 * Math.PI * frequency * generatorPeriodTime))
                    ));
            }
            return amplitudeSamples.Average();
        }
    }
}
