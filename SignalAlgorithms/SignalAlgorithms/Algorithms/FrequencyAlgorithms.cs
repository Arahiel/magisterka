﻿using SignalAlgorithms.Helpers;
using System;
using System.Collections.Generic;

namespace SignalAlgorithms.Algorithms
{
    public class FrequencyAlgorithms
    {
        private int _generatorSamplingRate;
        public FrequencyAlgorithms(int generatorSamplingRate)
        {
            _generatorSamplingRate = generatorSamplingRate;
        }
        public int CountingPulses(List<SignalSample> signalSamples)
        {
            int pulses = 0;
            bool canFinish = false;

            //Algorithm
            for (var i = 1; i <= signalSamples.Count; i++)
            {
                if (RisingEdgeThroughZero(signalSamples[i - 1], signalSamples[i]) && !canFinish)
                {
                    canFinish = true;
                    pulses = 0;
                }
                else if (FallingEdgeThroughZero(signalSamples[i - 1], signalSamples[i]) && canFinish)
                {
                    break;
                }
                else
                {
                    pulses++;
                }
            }

            return (int)Math.Round(1 / (1 / (double)_generatorSamplingRate * 2 * pulses)); // f = 1 / signal time period. Multiplied by 2, because only half of time period is needed.
        }


        private bool RisingEdgeThroughZero(SignalSample previousSample, SignalSample actualSample)
        {
            if( actualSample.Value > 0 && 
                actualSample.Value - previousSample.Value > actualSample.Value + previousSample.Value)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool FallingEdgeThroughZero(SignalSample previousSample, SignalSample actualSample)
        {
            if (actualSample.Value < 0 &&
                actualSample.Value + previousSample.Value > actualSample.Value - previousSample.Value)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
