﻿using SignalAlgorithms.Bases;
using SignalAlgorithms.Enums;
using SignalAlgorithms.Helpers;
using System;
using System.Collections.Generic;

namespace SignalAlgorithms.Algorithms
{
    public class PhaseAlgorithms
    {
        private int _generatorSamplingRate;
        public PhaseAlgorithms(int generatorSamplingRate)
        {
            _generatorSamplingRate = generatorSamplingRate;
        }

        public double UseSignalDerivative(List<SignalSample> signalSamples, int frequency, int sample) //Get phase of "sample"
        {
            var sumOfSamples = signalSamples[sample - 1].Value + signalSamples[sample - 2].Value;
            var subOfSamples = signalSamples[sample - 1].Value - signalSamples[sample - 2].Value;
            var tan = Math.Tan(2 * Math.PI * frequency * 1 / _generatorSamplingRate / 2); //tg(wTi/2)
            var gamma = Math.Atan2(sumOfSamples * tan, subOfSamples);
            var angle = gamma * 180 / Math.PI; //Convert radians to angle degrees
            return angle;
        }

        public int UseSignalDelay(GeneratorBase generator, int frequency, int sample) //Get phase of "sample"
        {
            //Algorithm needs 2 signals: the original one with desired frequency and the same, but delayed by Pi/2
            var sine = generator.GenerateSignal(frequency, 1, sample, SignalTypes.Sine);
            var delayedSine = generator.GenerateSignal(frequency, 1, sample, SignalTypes.Sine, 0.25);

            //The phase for desired sample is arcTg(sineValue/delayedSineValue)
            var gamma = Math.Atan2(sine[sample - 1].Value, delayedSine[sample - 1].Value);
            var angle = gamma * 180 / Math.PI; //Convert radians to angle degrees
            return (int)Math.Round(angle);
        }

        public int UseCoincidenceTime(List<SignalSample> actualSignal, List<SignalSample> referenceSignal, int frequencyOfSignals)
        {
            /*As in normal conditions there wouldn't set of signal samples, but continous signal, there wouldn't be then difference in amount of samples
            between signals. So here I assume that amount of samples are always the same, so I iterate through only one signal.

            I assume also that I'm checking signals with the same frequency.
            */
            bool checkReferenceSignal = false;
            int samplesCount = 0, sign = 0;

            for (var i = 1; i <= actualSignal.Count; i++)
            {
                if (!checkReferenceSignal)
                {
                    if (SignalGoesThroughZero(actualSignal[i - 1], actualSignal[i]))
                    {
                        var deriv = Derivative(actualSignal[i - 1], actualSignal[i]);
                        if(Math.Sign(deriv).Equals(Math.Sign(referenceSignal[i].Value)))
                        {
                            sign = 1;
                        }
                        else
                        {
                            sign = -1;
                        }
                        checkReferenceSignal = true;
                    }
                }
                else
                {
                    if (SignalGoesThroughZero(referenceSignal[i - 1], referenceSignal[i]))
                    {
                        samplesCount++;
                        break;
                    }
                    else
                    {
                        samplesCount++;
                    }
                }
            }

            var coincidenceTime = samplesCount / (double)_generatorSamplingRate;
            if(sign > 0)
            {
                return (int)(180 - Math.Round(sign * 2 * Math.PI * frequencyOfSignals * coincidenceTime * 180 / Math.PI));
            }
            else
            {
                return (int)(Math.Round(sign * 2 * Math.PI * frequencyOfSignals * coincidenceTime * 180 / Math.PI));
            }
        }

        private double Derivative(SignalSample previousSample, SignalSample actualSample)
        {
            return (actualSample.Value - previousSample.Value) / (actualSample.TimeOfAcquisition - previousSample.TimeOfAcquisition);
        }

        private bool SignalGoesThroughZero(SignalSample signalSample1, SignalSample signalSample2)
        {
            return Math.Sign(signalSample1.Value) != Math.Sign(signalSample2.Value);
        }
    }
}
