﻿using SignalAlgorithms.Enums;
using SignalAlgorithms.Helpers;
using SignalAlgorithms.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SignalAlgorithms.Bases
{
    public abstract class GeneratorBase : ISignalGenerator
    {
        public abstract int samplingRate { get; set; }
        public abstract List<SignalSample> GenerateSignal(int frequency, double amplitude, int samplesCount, SignalTypes signalType, double delay = 0, int phase = 0);

    }
}
