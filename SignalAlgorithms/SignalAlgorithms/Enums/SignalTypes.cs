﻿namespace SignalAlgorithms.Enums
{
    public enum SignalTypes
    {
        Sine,
        Square,
        Triangle,
        Ramp
    }
}
