﻿using SCPIns;
using SignalAlgorithms.Bases;
using SignalAlgorithms.Enums;
using SignalAlgorithms.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;

namespace SignalAlgorithms.Generators
{
    public class SCPI : GeneratorBase
    {
        public override int samplingRate { get; set; }

        private SCPICommands scpiCommands = new SCPICommands();
        private Instrument generator;
        private Instrument multimeter;

        public SCPI()
        {
            samplingRate = int.Parse(GetConfigValue("MultimeterSampleRate"));
            var generatorProtocolIndex = int.Parse(GetConfigValue("GeneratorProtocolIndex"));
            var generatorAddress = int.Parse(GetConfigValue("GeneratorAddress"));
            var multimeterProtocolIndex = int.Parse(GetConfigValue("MultimeterProtocolIndex"));
            var multimeterAddress = int.Parse(GetConfigValue("MultimeterAddress"));

            generator = new Instrument(generatorProtocolIndex, generatorAddress, string.Empty, string.Empty);
            multimeter = new Instrument(multimeterProtocolIndex, multimeterAddress, string.Empty, string.Empty);
        }

        public override List<SignalSample> GenerateSignal(int frequency, double amplitude, int samplesCount, SignalTypes signalType, double delay = 0, int phase = 0)
        {
            if(generator.Connect())
            {
                SendSignal(frequency, amplitude, signalType, phase);
            }
            else
            {
                throw new IOException("Could not connect to the generator");
            }

            if (multimeter.Connect())
            {
                return ReadSignalSamples(frequency, amplitude, samplesCount, delay);
            }
            else
            {
                throw new IOException("Could not connect to the multimeter");
            }
        }

        private List<SignalSample> ReadSignalSamples(int frequency, double amplitude, int samplesCount, double delay)
        {
            scpiCommands.Range = Math.Ceiling(amplitude).ToString();
            scpiCommands.Resolution = "0.1";

            List<SignalSample> signalSamples = new List<SignalSample>();
            DateTime startTime = DateTime.Now;
            TimeSpan sampleAcquisitionTime;

            Thread.Sleep((int)(delay / frequency * 1000));

            for (int i = 0; i < samplesCount; i++)
            {
                sampleAcquisitionTime = DateTime.Now - startTime;
                multimeter.formattedObj.WriteString(scpiCommands.MeasureVoltageAC());
                var sampleValue = double.Parse(multimeter.formattedObj.ReadString());

                signalSamples.Add(new SignalSample(sampleValue, sampleAcquisitionTime.TotalMilliseconds / 1000));
            }

            signalSamples.Select(x => x.TimeOfAcquisition -= signalSamples.First().TimeOfAcquisition);

            return signalSamples;
        }

        private void SendSignal(int frequency, double amplitude, SignalTypes signalType, int phase)
        {
            switch (signalType)
            {
                case SignalTypes.Sine:
                    scpiCommands.FUNC = "SIN";
                    break;
                case SignalTypes.Square:
                    scpiCommands.FUNC = "SQU";
                    break;
                case SignalTypes.Ramp:
                    scpiCommands.FUNC = "RAMP";
                    break;
                case SignalTypes.Triangle:
                    scpiCommands.FUNC = "TRI";
                    break;
            }
            scpiCommands.FREQ = frequency.ToString();
            scpiCommands.VOLT = amplitude.ToString();
            scpiCommands.OFFS = phase.ToString();

            generator.formattedObj.WriteString(scpiCommands.Apply());
        }

        public string GetConfigValue(string key)
        {
            return ConfigurationManager
            .OpenExeConfiguration(Assembly.GetExecutingAssembly().Location)
                .AppSettings.Settings[key].Value;
        }
    }
}
