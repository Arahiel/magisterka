﻿using SignalAlgorithms.Bases;
using SignalAlgorithms.Enums;
using SignalAlgorithms.Helpers;
using System;
using System.Collections.Generic;

namespace SignalAlgorithms.Generators
{
    public class SignalSimulation : GeneratorBase
    {
        public override int samplingRate { get; set; } = 12582912; // 12 MHz

        public SignalSimulation() { }
        public SignalSimulation(int sampleRate)
        {
            samplingRate = sampleRate;
        }


        public override List<SignalSample> GenerateSignal(int frequency, double amplitude, int samplesCount, SignalTypes signalType, double delay = 0, int phase = 0)
        {
            List<SignalSample> outputSignalSamples = new List<SignalSample>();

            switch (signalType)
            {
                case SignalTypes.Sine:
                    for (int i = 0; i < samplesCount; i++)
                    {
                        double time = i / (double)samplingRate;
                        outputSignalSamples.Add(new SignalSample(amplitude * Math.Sin(2 * Math.PI * i * frequency / samplingRate + phase * Math.PI / 180 - 2 * Math.PI * delay), time));
                    }
                    break;
                case SignalTypes.Square:
                    for (int i = 0; i < samplesCount; i++)
                    {
                        double time = i / (double)samplingRate;
                        outputSignalSamples.Add(new SignalSample(amplitude * (Math.Sin(2 * Math.PI * i * frequency / samplingRate + phase * Math.PI / 180 - 2 * Math.PI * delay) >= 0 ? 1 : -1), time));
                    }
                    break;
                case SignalTypes.Triangle:
                    throw new NotImplementedException();
                    break;
                case SignalTypes.Ramp:
                    throw new NotImplementedException();
                    break;
            }

            return outputSignalSamples;
        }

    }
}
