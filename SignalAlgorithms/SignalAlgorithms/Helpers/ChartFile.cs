﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SignalAlgorithms.Helpers
{
    public class ChartFile
    {
        public string ChartFileName { get; set; }

        public List<ChartValue> ChartValues { get; set; }

        public ChartFile(string chartFileName, ChartValue chartValue)
        {
            ChartFileName = chartFileName;
            ChartValues = new List<ChartValue>();
            ChartValues.Add(chartValue);
        }
    }
}
