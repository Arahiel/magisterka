﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SignalAlgorithms.Helpers
{
    public static class ChartHelper
    {
        public static List<ChartFile> listOfChartFilesWithValues = new List<ChartFile>();
        public static void ClearAll()
        {
            listOfChartFilesWithValues = new List<ChartFile>();
        }
        public static void AddElementToChartFile(string chartFileName, ChartValue chartValue)
        {
            ChartFile searchedChartFile;
            try
            {
                searchedChartFile = listOfChartFilesWithValues.First(x => x.ChartFileName.Equals(chartFileName));
                searchedChartFile.ChartValues.Add(chartValue);
            }
            catch
            {
                listOfChartFilesWithValues.Add(new ChartFile(chartFileName, chartValue));
            }
        }
        public static void GenerateFiles(string generatorName)
        {
            foreach (var chartFile in listOfChartFilesWithValues)
            {
                using (var stream = File.CreateText(Path.Combine(Directory.GetCurrentDirectory(), $"{generatorName}.{chartFile.ChartFileName}.csv")))
                {
                    foreach (var chartValue in chartFile.ChartValues)
                    {
                        stream.Write(chartValue.DifferenceRatio + ";");
                    }
                    stream.WriteLine();
                    foreach (var chartValue in chartFile.ChartValues)
                    {
                        stream.Write(chartValue.ReferenceValue + ";");
                    }
                    stream.WriteLine();
                };
            }
        }
    }
}
