﻿namespace SignalAlgorithms.Helpers
{
    public class ChartValue
    {
        public double DifferenceRatio { get; set; }
        public double ReferenceValue { get; set; }

        public ChartValue(double differenceRatio, double referenceValue)
        {
            DifferenceRatio = differenceRatio;
            ReferenceValue = referenceValue;
        }
        public ChartValue(double differenceRatio, int referenceValue)
        {
            DifferenceRatio = differenceRatio;
            ReferenceValue = referenceValue;
        }
    }
}
