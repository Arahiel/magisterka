﻿namespace SignalAlgorithms.Helpers
{
    public class SignalSample
    {
        public double Value { get; set; }
        public double TimeOfAcquisition { get; set; }

        public SignalSample(double value, double timeOfAcquisition)
        {
            Value = value;
            TimeOfAcquisition = timeOfAcquisition;
        }
    }
}
