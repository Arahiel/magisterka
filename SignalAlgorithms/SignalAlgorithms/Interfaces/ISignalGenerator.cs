﻿using SignalAlgorithms.Enums;
using SignalAlgorithms.Helpers;
using System.Collections.Generic;

namespace SignalAlgorithms.Interfaces
{
    public interface ISignalGenerator
    {
        List<SignalSample> GenerateSignal(int frequency, double amplitude, int samplesCount, SignalTypes signalType, double delay = 0, int phase = 0);
    }
}
