﻿using System.Collections.Generic;

namespace SCPIns
{
    public class SCPICommands
    {
        public List<string> Log = new List<string>();
        
        //GENERATE PROPERTIES
        public string FUNC  { get; set; }       //Function
        public string FREQ  { get; set; }       //Frequency  
        public string VOLT  { get; set; }       //Voltage 
        public string OFFS  { get; set; }       //Offset

        public string MType { get; set; }       //Type of modulation
        public string MFUNC { get; set; }       //Modulation function
        public string MFREQ { get; set; }       //Modulation frequency
        public string DEPT  { get; set; }       //Depth of AM modulation in %
        public string DEV   { get; set; }       //Deviation of FM modulation in Hz
        public string MSTAT { get; set; }       //State of modulations

        public string SSTAT { get; set; }       //State of sweep mode
        public string START { get; set; }       //Start frequency of sweep mode
        public string STOP  { get; set; }       //Stop frequency of sweep mode
        public string TIME  { get; set; }       //Time of sweep in seconds
        public string DELAY { get; set; }       //Trigger delay time


        //MEASUREMENT PROPERTIES
        public string Range { get; set; }       //Range of measured signal
        public string Resolution { get; set; }  //Resolution of measured signal
        public string NPLCycles { get; set; }   //Integration time (samples for input signal)
        public string MeasureType { get; set; } //Type of measurement for configure 
        public string SampleCount { get; set; } //Numbers of samples per trigger


        public string CheckError()
        {
            var com = "SYST:ERR?";
            Log.Add(com);
            return com;
        }

        public string Apply()
        {
            var com = "APPL:" + FUNC + " " + FREQ + ", " + VOLT + ", " + OFFS;
            Log.Add(com);
            return com;
        }

        public string Function()
        {
            var com = "FUNC:SHAP " + FUNC;
            Log.Add(com);
            return com;
        }

        public string Frequency()
        {
            var com = "FREQ " + FREQ;
            Log.Add(com);
            return com;
        }

        public string Voltage()
        {
            var com = "VOLT " + VOLT;
            Log.Add(com);
            return com;
        }

        public string VoltageOffset()
        {
            var com = "VOLT:OFFS " + OFFS;
            Log.Add(com);
            return com;
        }

        public string Depth()
        { 
            var com = "AM:DEPT " + DEPT;
            Log.Add(com);
            return com;
        }

        public string Deviation()
        {
            var com = "FM:DEV " + DEV;
            Log.Add(com);
            return com;
        }

        public string ModulationFunction()
        {
            var com = MType + ":INT:FUNC " + MFUNC;
            Log.Add(com);
            return com;
        }

        public string ModulationFrequency()
        {
            var com = MType + ":INT:FREQ " + MFREQ;
            Log.Add(com);
            return com;
        }

        public string ModulationState()
        {
            var com = MType + ":STAT " + MSTAT;
            Log.Add(com);
            return com;
        }

        public string SweepFrequencyStart()
        {
            var com = "FREQ:STAR " + START;
            Log.Add(com);
            return com;
        }

        public string SweepFrequencyStop()
        {
            var com = "FREQ:STOP " + STOP;
            Log.Add(com);
            return com;
        }

        public string SweepTime()
        {
            var com = "SWE:TIME " + TIME;
            Log.Add(com);
            return com;
        }

        public string SweepState()
        {
            var com = "SWE:STAT " + SSTAT;
            Log.Add(com);
            return com;
        }

        public string MeasureVoltageAC()
        {
            var com = "MEAS:VOLT:AC? " + Range + "," + Resolution;
            Log.Add(com);
            return com;
        }

        public string MeasureVoltage()
        {
            var com = "MEAS:VOLT:DC? " + Range + "," + Resolution;
            Log.Add(com);
            return com;
        }

        public string MeasureFrequency()
        {
            var com = "MEAS:FREQ? " + Range + "," + Resolution;
            Log.Add(com);
            return com;
        }

        public string ConfigureVoltageAC()
        {
            var com = "CONF:VOLT:AC " + Range + "," + Resolution;
            Log.Add(com);
            return com;
        }

        public string ConfigureVoltage()
        {
            var com = "CONF:VOLT:DC " + Range + "," + Resolution;
            Log.Add(com);
            return com;
        }

        public string ConfigureFrequency()
        {
            var com = "CONF:FREQ " + Range + "," + Resolution;
            Log.Add(com);
            return com;
        }

        public string VoltageNPLC()
        {
            var com = "SENS:VOLT:DC:NPLC " + NPLCycles;
            Log.Add(com);
            return com;
        }

        public string MeasureInitiate()
        {
            var com = "INIT";
            Log.Add(com);
            return com;
        }

        public string Fetch()
        {
            var com = "FETC?";
            Log.Add(com);
            return com;
        }

        public string Read()
        {
            var com = "READ?";
            Log.Add(com);
            return com;
        }

        public string TriggerDelay()
        {
            var com = "TRIG:DEL " + DELAY;
            Log.Add(com);
            return com;
        }

        public string TriggerDelayCheck()
        {
            var com = "TRIG:DEL?";
            Log.Add(com);
            return com;
        }

        public string SetSamples()
        {
            var com = "SAMP:COUN " + SampleCount;
            Log.Add(com);
            return com;
        }

        public string Reset()
        {
            var com = "*RST";
            Log.Add(com);
            return com;
        }

        public string TriggerSourceBus()
        {
            var com = "TRIG:SOUR BUS";
            Log.Add(com);
            return com;
        }

    }
}
