﻿using Ivi.Visa.Interop;
using System;

namespace SCPIns
{
    public class Instrument
    {
        
        public ResourceManager resourceManager { get; set; }
        public FormattedIO488 formattedObj { get; set; }
        public int Address { get; set; }
        public bool Status { get; set; }
        public object[] ReturnMessages { get; set; }
        public string ReturnMessage { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public int ProtocolIndex { get; set; }

        public Instrument(int protocol, int address, string man, string model)
        {
            resourceManager = new ResourceManager();
            formattedObj = new FormattedIO488();
            Address = address;
            Status = false;
            ReturnMessages = null;
            ReturnMessage = string.Empty;
            Manufacturer = man;
            Model = model;
            ProtocolIndex = protocol;
        }

        public override string ToString()
        {
            return string.Format("GPIB{0}::{1} - {2} - {3}\n", ProtocolIndex, Address, Manufacturer, Model);  
        }


        //Test connection with instrument.
        public bool Connect()
        {
            try
            {
                formattedObj.IO = (IMessage)resourceManager.Open("GPIB" + ProtocolIndex + "::" + Address + "::INSTR", AccessMode.NO_LOCK, 0, "");
                Status = true;
            }
            catch (Exception ioex)
            {
                Console.WriteLine(ioex.Message);
            }
            return Status;
        }

        public void Identify()
        {
            try
            {
                if (Status)
                {
                    formattedObj.WriteString("*IDN?", true);
                    ReturnMessages = (object[])formattedObj.ReadList(IEEEASCIIType.ASCIIType_Any, ",");
                    if (ReturnMessages != null)
                    {
                        Manufacturer = ReturnMessages[0].ToString();
                        Model = ReturnMessages[1].ToString();
                    }
                }
            }
            catch (Exception ioex)
            {
                Console.WriteLine(ioex.Message);
            }
        }

        //Function to release IO instrument.
        public void ReleaseInstrument()
        {
            try { formattedObj.IO.Close(); }
            catch { }
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(formattedObj);
            }
            catch { }
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(formattedObj);
            }
            catch { }
        }

        public void CheckErrors(SCPICommands comms)
        {
            ReturnMessage = string.Empty;
            do
            {
                formattedObj.WriteString(comms.CheckError());
                if (formattedObj.ReadString() != null)
                {
                    ReturnMessage = formattedObj.ReadString();
                    Console.WriteLine(ReturnMessage + "\n");
                }
            } while (ReturnMessage != string.Empty);
        }
    }
}
