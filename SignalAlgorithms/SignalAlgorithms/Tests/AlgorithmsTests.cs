using NUnit.Framework;
using SignalAlgorithms.Algorithms;
using SignalAlgorithms.Bases;
using SignalAlgorithms.Enums;
using SignalAlgorithms.Generators;
using SignalAlgorithms.Helpers;
using System;
using System.Collections.Generic;

namespace SignalAlgorithms.Tests
{
    [TestFixture(typeof(SignalSimulation))]
    [TestFixture(typeof(SCPI))]
    public class AlgorithmsTests<TGenerator> where TGenerator : GeneratorBase, new()
    {
        private GeneratorBase _signalGenerator;
        private readonly double WARNING_THRESHOLD = 0.1;
        private readonly double ERROR_THRESHOLD = 0.3;

        [OneTimeSetUp]
        public void SetUp()
        {
            _signalGenerator = new TGenerator();
            ChartHelper.ClearAll();
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            ChartHelper.GenerateFiles(_signalGenerator.GetType().Name);
        }

        [TestCaseSource(nameof(FrequencyTestInputs))]
        public void CountingSignalPulsesShouldProvideSignalFrequency(int referenceFrequency, int referenceAmplitude, int referenceSamplesCount, SignalTypes signalType)
        {
            //Arrange
            var signalSamples = _signalGenerator.GenerateSignal(referenceFrequency, referenceAmplitude, referenceSamplesCount, signalType);
            FrequencyAlgorithms algorithms = new FrequencyAlgorithms(_signalGenerator.samplingRate);

            //Act
            var outputFrequency = algorithms.CountingPulses(signalSamples);
            var difference = Math.Abs(referenceFrequency - outputFrequency);
            double diffPercentage = (double)difference / referenceFrequency;

            //Save chart value
            ChartHelper.AddElementToChartFile(TestContext.CurrentContext.Test.MethodName, new ChartValue(diffPercentage, referenceFrequency));

            //Assert
            if (diffPercentage < WARNING_THRESHOLD)
            {
                Assert.Pass($"Difference: {difference}");
            }
            else if (diffPercentage < ERROR_THRESHOLD)
            {
                Assert.Warn($"Difference: {difference}");
            }
            else
            {
                Assert.Fail($"Difference: {difference}");
            }
        }

        [TestCaseSource(nameof(DerivativePhaseTestInputsByFrequency))]
        public void UsingSignalDerivativeShouldProvidePhaseByFrequency(int sample, int referenceFrequency, int amplitude, int sampleCount, SignalTypes signalType, double phase)
        {
            //Arrange
            PhaseAlgorithms algorithms = new PhaseAlgorithms(_signalGenerator.samplingRate);
            var signalSamples = _signalGenerator.GenerateSignal(referenceFrequency, amplitude, sampleCount, signalType);

            //Act
            var outputPhase = algorithms.UseSignalDerivative(signalSamples, referenceFrequency, sample);
            var expectedPhaseDifferenceBetweenSamples = (double)360 * referenceFrequency / _signalGenerator.samplingRate / 2;
            double expectedPhase = phase - expectedPhaseDifferenceBetweenSamples;
            var difference = Math.Abs(expectedPhase - outputPhase);
            double diffPercentage = Math.Abs(difference / expectedPhase);

            //Save chart value
            ChartHelper.AddElementToChartFile(TestContext.CurrentContext.Test.MethodName, new ChartValue(diffPercentage, referenceFrequency)); ;

            //Assert
            if (diffPercentage < WARNING_THRESHOLD)
            {
                Assert.Pass($"Difference: {difference}");
            }
            else if (diffPercentage < ERROR_THRESHOLD)
            {
                Assert.Warn($"Difference: {difference}");
            }
            else
            {
                Assert.Fail($"Difference: {difference}");
            }
        }

        [TestCaseSource(nameof(DerivativePhaseTestInputsByPhase))]
        public void UsingSignalDerivativeShouldProvidePhaseByPhase(int sample, int frequency, int amplitude, int sampleCount, SignalTypes signalType, double referencePhase)
        {
            //Arrange
            PhaseAlgorithms algorithms = new PhaseAlgorithms(_signalGenerator.samplingRate);
            var signalSamples = _signalGenerator.GenerateSignal(frequency, amplitude, sampleCount, signalType);

            //Act
            var outputPhase = algorithms.UseSignalDerivative(signalSamples, frequency, sample);
            var expectedPhaseDifferenceBetweenSamples = (double)360 * frequency / _signalGenerator.samplingRate / 2;
            double expectedPhase = referencePhase - expectedPhaseDifferenceBetweenSamples;
            var difference = Math.Abs(expectedPhase - outputPhase);
            double diffPercentage = Math.Abs(difference / expectedPhase);

            //Save chart value
            ChartHelper.AddElementToChartFile(TestContext.CurrentContext.Test.MethodName, new ChartValue(diffPercentage, referencePhase)); ;

            //Assert
            if (diffPercentage < WARNING_THRESHOLD)
            {
                Assert.Pass($"Difference: {difference}");
            }
            else if (diffPercentage < ERROR_THRESHOLD)
            {
                Assert.Warn($"Difference: {difference}");
            }
            else
            {
                Assert.Fail($"Difference: {difference}");
            }
        }

        [TestCaseSource(nameof(PhaseTestInputs))]
        public void MeasuringArcTgBetweenSignalAndDelayedOneShouldProvidePhase(int sample, int expectedPhase)
        {
            //Arrange
            const int generatorSamplingRate = 20;
            SignalSimulation customGenerator = new SignalSimulation(generatorSamplingRate);
            PhaseAlgorithms algorithms = new PhaseAlgorithms(generatorSamplingRate);

            //Act
            var outputPhase = algorithms.UseSignalDelay(customGenerator, 1, sample);

            //Assert
            Assert.That(outputPhase, Is.EqualTo(expectedPhase));
        }

        [TestCaseSource(nameof(TwoSignalsPhaseTestInputsByFrequency))]
        public void MeasuringCoincidenceTimeBetweenSignalsShouldProvidePhaseDifferenceByFrequency(int referenceFrequency, int amplitude, int samplesCount, SignalTypes signalType, int phase)
        {
            //Arrange
            PhaseAlgorithms algorithms = new PhaseAlgorithms(_signalGenerator.samplingRate);

            var baseSignalSamples = _signalGenerator.GenerateSignal(referenceFrequency, amplitude, samplesCount, signalType);
            var referenceSignalSamples = _signalGenerator.GenerateSignal(referenceFrequency, amplitude, samplesCount, signalType, phase: phase);

            ////Act

            var outputPhase = algorithms.UseCoincidenceTime(baseSignalSamples, referenceSignalSamples, referenceFrequency);
            var difference = Math.Abs(phase - outputPhase);
            double diffPercentage = Math.Abs((double)difference / phase);

            //Save chart value
            ChartHelper.AddElementToChartFile(TestContext.CurrentContext.Test.MethodName, new ChartValue(diffPercentage, referenceFrequency));

            //Assert
            if (diffPercentage < WARNING_THRESHOLD)
            {
                Assert.Pass($"Difference: {difference}");
            }
            else if (diffPercentage < ERROR_THRESHOLD)
            {
                Assert.Warn($"Difference: {difference}");
            }
            else
            {
                Assert.Fail($"Difference: {difference}");
            }
        }

        [TestCaseSource(nameof(TwoSignalsPhaseTestInputsByPhase))]
        public void MeasuringCoincidenceTimeBetweenSignalsShouldProvidePhaseDifferenceByPhase(int frequency, int amplitude, int samplesCount, SignalTypes signalType, int referencePhase)
        {
            //Arrange
            PhaseAlgorithms algorithms = new PhaseAlgorithms(_signalGenerator.samplingRate);

            var baseSignalSamples = _signalGenerator.GenerateSignal(frequency, amplitude, samplesCount, signalType);
            var referenceSignalSamples = _signalGenerator.GenerateSignal(frequency, amplitude, samplesCount, signalType, phase: referencePhase);

            ////Act

            var outputPhase = algorithms.UseCoincidenceTime(baseSignalSamples, referenceSignalSamples, frequency);
            var difference = Math.Abs(referencePhase - outputPhase);
            double diffPercentage = Math.Abs((double)difference / referencePhase);

            //Save chart value
            ChartHelper.AddElementToChartFile(TestContext.CurrentContext.Test.MethodName, new ChartValue(diffPercentage, referencePhase));

            //Assert
            if (diffPercentage < WARNING_THRESHOLD)
            {
                Assert.Pass($"Difference: {difference}");
            }
            else if (diffPercentage < ERROR_THRESHOLD)
            {
                Assert.Warn($"Difference: {difference}");
            }
            else
            {
                Assert.Fail($"Difference: {difference}");
            }
        }

        [TestCaseSource(nameof(AmplitudeTestInputs))]
        public void UsingSignalDelayShouldProvideAmplitudeByAmplitude(int frequency, double referenceAmplitude, int samplesCount, SignalTypes signalType)
        {
            //Arrange
            AmplitudeAlgorithms algorithms = new AmplitudeAlgorithms(_signalGenerator.samplingRate);

            var signalSamples = _signalGenerator.GenerateSignal(frequency, referenceAmplitude, samplesCount, signalType);

            ////Act

            var outputAmplitude = algorithms.UseSignalDelay(signalSamples, frequency);
            var difference = Math.Abs(referenceAmplitude - outputAmplitude);
            double diffPercentage = Math.Abs(difference / referenceAmplitude);

            //Save chart value
            ChartHelper.AddElementToChartFile(TestContext.CurrentContext.Test.MethodName, new ChartValue(diffPercentage, referenceAmplitude));

            //Assert
            if (diffPercentage < WARNING_THRESHOLD)
            {
                Assert.Pass($"Difference: {difference}");
            }
            else if (diffPercentage < ERROR_THRESHOLD)
            {
                Assert.Warn($"Difference: {difference}");
            }
            else
            {
                Assert.Fail($"Difference: {difference}");
            }
        }

        [TestCaseSource(nameof(FrequencyTestInputs))]
        public void UsingSignalDelayShouldProvideAmplitudeByFrequency(int referenceFrequency, double amplitude, int samplesCount, SignalTypes signalType)
        {
            //Arrange
            AmplitudeAlgorithms algorithms = new AmplitudeAlgorithms(_signalGenerator.samplingRate);

            var signalSamples = _signalGenerator.GenerateSignal(referenceFrequency, amplitude, samplesCount, signalType);

            ////Act

            var outputAmplitude = algorithms.UseSignalDelay(signalSamples, referenceFrequency);
            var difference = Math.Abs(amplitude - outputAmplitude);
            double diffPercentage = Math.Abs(difference / amplitude);

            //Save chart value
            ChartHelper.AddElementToChartFile(TestContext.CurrentContext.Test.MethodName, new ChartValue(diffPercentage, referenceFrequency));

            //Assert
            if (diffPercentage < WARNING_THRESHOLD)
            {
                Assert.Pass($"Difference: {difference}");
            }
            else if (diffPercentage < ERROR_THRESHOLD)
            {
                Assert.Warn($"Difference: {difference}");
            }
            else
            {
                Assert.Fail($"Difference: {difference}");
            }
        }

        public static IEnumerable<TestCaseData> DerivativePhaseTestInputsByPhase
        {
            get
            {
                yield return new TestCaseData(351, 1000, 1, 13000, SignalTypes.Sine, 10);
                yield return new TestCaseData(701, 1000, 1, 13000, SignalTypes.Sine, 20);
                yield return new TestCaseData(1050, 1000, 1, 13000, SignalTypes.Sine, 30);
                yield return new TestCaseData(1400, 1000, 1, 13000, SignalTypes.Sine, 40);
                yield return new TestCaseData(1749, 1000, 1, 13000, SignalTypes.Sine, 50);
                yield return new TestCaseData(2099, 1000, 1, 13000, SignalTypes.Sine, 60);
                yield return new TestCaseData(2448, 1000, 1, 13000, SignalTypes.Sine, 70);
                yield return new TestCaseData(2798, 1000, 1, 13000, SignalTypes.Sine, 80);
                yield return new TestCaseData(3146, 1000, 1, 13000, SignalTypes.Sine, 90);
                yield return new TestCaseData(3497, 1000, 1, 13000, SignalTypes.Sine, 100);
                yield return new TestCaseData(3846, 1000, 1, 13000, SignalTypes.Sine, 110);
                yield return new TestCaseData(6642, 1000, 1, 13000, SignalTypes.Sine, -170);
                yield return new TestCaseData(6992, 1000, 1, 13000, SignalTypes.Sine, -160);
                yield return new TestCaseData(7342, 1000, 1, 13000, SignalTypes.Sine, -150);
                yield return new TestCaseData(7691, 1000, 1, 13000, SignalTypes.Sine, -140);
                yield return new TestCaseData(8041, 1000, 1, 13000, SignalTypes.Sine, -130);
                yield return new TestCaseData(8390, 1000, 1, 13000, SignalTypes.Sine, -120);
                yield return new TestCaseData(8740, 1000, 1, 13000, SignalTypes.Sine, -110);
                yield return new TestCaseData(9089, 1000, 1, 13000, SignalTypes.Sine, -100);
                yield return new TestCaseData(9439, 1000, 1, 13000, SignalTypes.Sine, -90);
                yield return new TestCaseData(9788, 1000, 1, 13000, SignalTypes.Sine, -80);
                yield return new TestCaseData(10138, 1000, 1, 13000, SignalTypes.Sine, -70);

            }
        }

        public static IEnumerable<TestCaseData> DerivativePhaseTestInputsByFrequency
        {
            get
            {
                yield return new TestCaseData(3145729, 1, 1, 13000000, SignalTypes.Sine, 90);
                yield return new TestCaseData(1572865, 2, 1, 7000000, SignalTypes.Sine, 90);
                yield return new TestCaseData(629146, 5, 1, 4000000, SignalTypes.Sine, 90);
                yield return new TestCaseData(314573, 10, 1, 1300000, SignalTypes.Sine, 90);
                yield return new TestCaseData(157287, 20, 1, 700000, SignalTypes.Sine, 90);
                yield return new TestCaseData(62915, 50, 1, 400000, SignalTypes.Sine, 90);
                yield return new TestCaseData(31458, 100, 1, 130000, SignalTypes.Sine, 90);
                yield return new TestCaseData(15729, 200, 1, 70000, SignalTypes.Sine, 90);
                yield return new TestCaseData(6292, 500, 1, 40000, SignalTypes.Sine, 90);
                yield return new TestCaseData(3146, 1000, 1, 13000, SignalTypes.Sine, 90);
                yield return new TestCaseData(1573, 2000, 1, 7000, SignalTypes.Sine, 90);
                yield return new TestCaseData(630, 5000, 1, 4000, SignalTypes.Sine, 90);
                yield return new TestCaseData(315, 10000, 1, 1300, SignalTypes.Sine, 90);
                yield return new TestCaseData(158, 20000, 1, 700, SignalTypes.Sine, 90);
                yield return new TestCaseData(63, 50000, 1, 400, SignalTypes.Sine, 90);
                yield return new TestCaseData(32, 100000, 1, 130, SignalTypes.Sine, 90);
                yield return new TestCaseData(16, 200000, 1, 70, SignalTypes.Sine, 90);
                yield return new TestCaseData(7, 500000, 1, 40, SignalTypes.Sine, 90);
                yield return new TestCaseData(4, 1000000, 1, 13, SignalTypes.Sine, 90);
            }
        }

        public static IEnumerable<TestCaseData> PhaseTestInputs
        {
            get
            {
                yield return new TestCaseData(1, 180);
                yield return new TestCaseData(2, 162);
                yield return new TestCaseData(3, 144);
                yield return new TestCaseData(4, 126);
                yield return new TestCaseData(5, 108);
                yield return new TestCaseData(6, 90);
                yield return new TestCaseData(7, 72);
                yield return new TestCaseData(8, 54);
                yield return new TestCaseData(9, 36);
                yield return new TestCaseData(10, 18);
                yield return new TestCaseData(11, 0);
            }
        }
        public static IEnumerable<TestCaseData> TwoSignalsPhaseTestInputsByPhase
        {
            get
            {
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, 1);
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, 2);
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, 5);
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, 10);
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, 20);
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, 40);
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, 50);
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, 60);
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, 30);
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, 70);
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, 80);
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, 90);
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, 100);
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, 110);
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, -1);
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, -2);
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, -5);
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, -10);
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, -20);
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, -40);
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, -50);
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, -60);
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, -30);
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, -70);
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, -80);
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, -90);
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, -100);
                yield return new TestCaseData(1000, 1, 1000000, SignalTypes.Sine, -110);
            }                                  
        }

        public static IEnumerable<TestCaseData> TwoSignalsPhaseTestInputsByFrequency
        {
            get
            {
                yield return new TestCaseData(1, 10, 20000000, SignalTypes.Sine, 11);
                yield return new TestCaseData(2, 10, 10000000, SignalTypes.Sine, 11);
                yield return new TestCaseData(5, 10, 10000000, SignalTypes.Sine, 11);
                yield return new TestCaseData(10, 10, 10000000, SignalTypes.Sine, 11);
                yield return new TestCaseData(20, 10, 10000000, SignalTypes.Sine, 11);
                yield return new TestCaseData(50, 10, 10000000, SignalTypes.Sine, 11);
                yield return new TestCaseData(100, 10, 10000000, SignalTypes.Sine, 11);
                yield return new TestCaseData(200, 10, 10000000, SignalTypes.Sine, 11);
                yield return new TestCaseData(500, 10, 10000000, SignalTypes.Sine, 11);
                yield return new TestCaseData(1000, 10, 10000000, SignalTypes.Sine, 11);
                yield return new TestCaseData(2000, 10, 10000000, SignalTypes.Sine, 11);
                yield return new TestCaseData(5000, 10, 10000000, SignalTypes.Sine, 11);
                yield return new TestCaseData(10000, 10, 10000000, SignalTypes.Sine, 11);
                yield return new TestCaseData(20000, 10, 10000000, SignalTypes.Sine, 11);
                yield return new TestCaseData(50000, 10, 10000000, SignalTypes.Sine, 11);
                yield return new TestCaseData(100000, 10, 10000000, SignalTypes.Sine, 11);
                yield return new TestCaseData(200000, 10, 10000000, SignalTypes.Sine, 11);
                yield return new TestCaseData(500000, 10, 10000000, SignalTypes.Sine, 11);
                yield return new TestCaseData(1000000, 10, 10000000, SignalTypes.Sine, 11);
            }
        }

        public static IEnumerable<TestCaseData> FrequencyTestInputs
        {
            get
            {
                yield return new TestCaseData(1, 10, 20000000, SignalTypes.Sine);
                yield return new TestCaseData(2, 10, 10000000, SignalTypes.Sine);
                yield return new TestCaseData(5, 10, 10000000, SignalTypes.Sine);
                yield return new TestCaseData(10, 10, 10000000, SignalTypes.Sine);
                yield return new TestCaseData(20, 10, 10000000, SignalTypes.Sine);
                yield return new TestCaseData(50, 10, 10000000, SignalTypes.Sine);
                yield return new TestCaseData(100, 10, 10000000, SignalTypes.Sine);
                yield return new TestCaseData(200, 10, 10000000, SignalTypes.Sine);
                yield return new TestCaseData(500, 10, 10000000, SignalTypes.Sine);
                yield return new TestCaseData(1000, 10, 10000000, SignalTypes.Sine);
                yield return new TestCaseData(2000, 10, 10000000, SignalTypes.Sine);
                yield return new TestCaseData(5000, 10, 10000000, SignalTypes.Sine);
                yield return new TestCaseData(10000, 10, 10000000, SignalTypes.Sine);
                yield return new TestCaseData(20000, 10, 10000000, SignalTypes.Sine);
                yield return new TestCaseData(50000, 10, 10000000, SignalTypes.Sine);
                yield return new TestCaseData(100000, 10, 10000000, SignalTypes.Sine);
                yield return new TestCaseData(200000, 10, 10000000, SignalTypes.Sine);
                yield return new TestCaseData(500000, 10, 10000000, SignalTypes.Sine);
                yield return new TestCaseData(1000000, 10, 10000000, SignalTypes.Sine);
            }
        }

        public static IEnumerable<TestCaseData> AmplitudeTestInputs
        {
            get
            {
                yield return new TestCaseData(5000, 1.1, 10000, SignalTypes.Sine);
                yield return new TestCaseData(5000, 2.02, 10000, SignalTypes.Sine);
                yield return new TestCaseData(5000, 5.005, 10000, SignalTypes.Sine);
                yield return new TestCaseData(5000, 10.0001, 10000, SignalTypes.Sine);
                yield return new TestCaseData(5000, 20.00002, 10000, SignalTypes.Sine);
                yield return new TestCaseData(5000, 50.000005, 10000, SignalTypes.Sine);
                yield return new TestCaseData(5000, 100, 10000, SignalTypes.Sine);
                yield return new TestCaseData(5000, 200, 10000, SignalTypes.Sine);
                yield return new TestCaseData(5000, 500, 10000, SignalTypes.Sine);
                yield return new TestCaseData(5000, 1000, 10000, SignalTypes.Sine);
                yield return new TestCaseData(5000, 2000, 10000, SignalTypes.Sine);
                yield return new TestCaseData(5000, 5000, 10000, SignalTypes.Sine);
                yield return new TestCaseData(5000, 10000, 10000, SignalTypes.Sine);
                yield return new TestCaseData(5000, 20000, 10000, SignalTypes.Sine);
                yield return new TestCaseData(5000, 50000, 10000, SignalTypes.Sine);
                yield return new TestCaseData(5000, 100000, 10000, SignalTypes.Sine);
                yield return new TestCaseData(5000, 200000, 10000, SignalTypes.Sine);
                yield return new TestCaseData(5000, 500000, 10000, SignalTypes.Sine);
                yield return new TestCaseData(5000, 1000000, 10000, SignalTypes.Sine);
            }
        }

    }
}