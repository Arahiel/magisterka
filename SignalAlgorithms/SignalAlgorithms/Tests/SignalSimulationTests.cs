﻿using NUnit.Framework;
using SignalAlgorithms.Enums;
using SignalAlgorithms.Generators;
using System.IO;
using System.Linq;

namespace SignalAlgorithms.Tests
{
    [TestFixture]
    public class SignalSimulationTests
    {
        [TestCase(10, 1, 100, SignalTypes.Sine, 0, 0)]
        [TestCase(1, 1, 100, SignalTypes.Square, 0, 0)]
        [TestCase(10, 1, 100, SignalTypes.Sine, 0, 90)]
        [TestCase(1, 1, 100, SignalTypes.Sine, 0, -90)]
        [TestCase(1, 1, 100, SignalTypes.Square, 0, 90)]
        [TestCase(1, 1, 100, SignalTypes.Square, 0, -90)]
        [TestCase(1, 1, 100, SignalTypes.Sine, 0.25, 0)]
        [TestCase(1, 1, 100, SignalTypes.Square, 0.25, 0)]
        [TestCase(10, 1, 100, SignalTypes.Sine, 0, 110)]
        [TestCase(1000, 1, 13000, SignalTypes.Sine, 0, 0)]
        public void WaveformTest(int frequency, int amplitude, int samplesCount, SignalTypes signalType, double delay, int phase)
        {
            SignalSimulation generator = new SignalSimulation(12582912);

            var samples = generator.GenerateSignal(frequency, amplitude, samplesCount, signalType, delay, phase);
            var timeStamps = samples.Select(x => x.TimeOfAcquisition);
            var values = samples.Select(x => x.Value);

            using (var stream = File.CreateText(Path.Combine(Directory.GetCurrentDirectory(), $"{signalType}.Delay{delay}.Phase{phase}.ChartValues.csv")))
            {
                foreach (var timeStamp in timeStamps)
                {
                    stream.Write(timeStamp + ";");
                }
                stream.WriteLine();
                foreach (var value in values)
                {
                    stream.Write(value + ";");
                }
                stream.WriteLine();
            };
        }
    }
}
